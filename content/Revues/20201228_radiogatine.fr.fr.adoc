= [Radio G&acirc;tine] Meta-Press.es la solution alternative &agrave; Google Actualit&eacute;s
:slug: 20201228_radiogatine.fr
:lang: fr
:date: 2020-12-28
:author: Siltaar

image::/images/20201214_radiogatine.fr_5fe1c6e31082d4.72030039.jpg[title="Simon D. dans son bureau © 2020 Samuel Pacault, Radio Gâtine"]

Et c’est à Pougne-Hérisson que ça se passe. En effet, Simon Descarpentries est développeur web pour le compte d’Acoeuro, une société de services en logiciels libres dont il est l’un des quatre associés. Depuis 2013, Simon a travaillé sur la création d’un moteur de recherche dédié à la presse. C’est accompagné de Christopher son apprenti, que ce Sarthois d’origine propose cette solution alternative et militante au fil d’actualités de Google. Baptisé Meta-press.es ce moteur de recherche dédié à la presse a officiellement été lancé en fin d’année dernière. Rencontré dans son bureau, au cœur de la Gâtine, l’entrepreneur de 36 ans nous présente ce projet devenu réalité. Portrait audio.

http://radiogatine.fr/news/meta-presse-la-solution-alternative-a-google-832[*Meta-Press.es la solution alternative à Google Actualités*]

Notez que Meta-press.es, le moteur de recherche dédié à la presse, est accessible via une extension du navigateur web Mozilla Firefox. Il fonctionne sans publicité, mais grâce aux dons que vous pouvez faire lorsque vous installerez le logiciel. Pour en savoir plus, une adresse, le : www.meta-press.es
