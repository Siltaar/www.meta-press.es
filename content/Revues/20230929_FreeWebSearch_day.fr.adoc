= [NLnet] Journée de la recherche libre sur le Web : rencontres
:slug: 20230929_freewebsearchday_meet_the_makers
:lang: fr
:date: 2023-09-29
:author: Siltaar
:toc:
:toc-title: Index

Interviews de Viktor Lofgren (du projet de moteur de recherche *Marginalia*) et Simon Descarpentries (fondateur du méta-moteur de recherche Meta-Press.es).

Le _https://freewebsearch.org/en/[FreeWebSearch Day]_ est un évènement annuel qui se tient autour du 29 septembre. C'est un jour pour faire vivre la liberté d'information et la démocratie. Tout le monde est invité à rejoindre le mouvement et organiser un évènement local et des actions sur ce thème.

Le projet *Meta-Press.es* de Simon Descarpentries vous permet d'explorer l'actualité en ligne sans intermédiaire entre votre ordinateur et les journaux en ligne consultés. Ce moteur de recherche se présente sous la forme d'une extension de navigateur web et vous aide à franchir les marécages de publicité, de _fake-news_ et de collecte de vos données personnelles en ligne. Avec Meta-Press.es, vous reprenez le contrôle de vos recherches et lectures en ligne en sélectionnant vos sources.

* https://nlnet.nl/events/20230929/index.html[Page de l'évènement] sur le site d'NLnet
* https://nlnet.nl/media/simon-metapress.opus[L'enregistrement audio] de l'interview (45 minutes)

== Entrevue avec Simon Descarpentries de Meta-Press.es

[quote,Simon Descarpentries,Meta-Press.es]
« Les moteurs de recherche ne nous libèrent pas de l'effort à faire pour connaître le monde »

Initialement publié sur : https://nlnet.nl/news/2023/20231012-meta-press.html

Le moteur de recherche https://www.meta-press.es/[Meta-Press.es] vous permet d'explorer la presse en ligne sans intermédiaire ni suivi publicitaire. Créateur et principal développeur, Simon Descarpentries est un fervent supporteur des logiciels libres depuis (plus de 20 ans). Il a été https://framasoft.org[employé de Framasoft] et est actuellement gérant de la SSII Acoeuro.com et trésorier du https://www.fdn2.org/en/presentation.html[Fond de défense de la neutralité du Net]. Nous l'avons interviewé pour le https://nlnet.nl/events/20230929/index.html[#FreeWebSearchDay]. Vous pouvez écouter l'enregistrement de cette entrevue ou lire la transcription suivante.

*Question : Pouvez-vous nous dire quelque chose à propos de Meta-Press.es ?*

Réponse : Meta-Press est un logiciel libre permettant à chacun de chercher dans la presse en ligne. En un clic vous pouvez chercher dans 930 journaux. Toutes ces sources sont indexées une par une par des humains. Actuellement, si vous faîtes une recherche sur la presse via le moteur de recherche d'un GAFAM vous risquez d'obtenir des résultats en provenance de faux journaux forgés sur mesure pour les berner. Certains se contentent de de copier du vrai contenu ailleurs et d'y coller leurs pub à côté. Les GAFAM ne font pas la différence car leur processus de découverte et d'indexation des journaux en ligne est entièrement automatisé. Avec Meta-Press.es, nous avons fait le travail à la main, donc tout est vérifié par des humains. De plus, Meta-Press.es intègre plus de 700 journaux donnant accès à leur contenu, parce que c'est le modèle économique qu'ils ont choisi. Contrairement à Google News, Meta-Press.es ne vous mène pas à un cul de sac en vous montrant surtout ce qui existe mais que vous ne pourrez pas lire (sans abonnement).

=== Pas de censure

Meta-press est une extension du navigateur web Firefox, accessible à tous. Elle est construite avec une architecture qui garanti qu'il n'y a pas de goulot d'étranglement, de point unique de passage pour les requêtes, d'où on peut facilement surveiller le trafic et en censurer une partie. Une fois l'extension Meta-Press.es installée dans votre navigateur, vos requêtes de recherche ne sont pas envoyées vers un hypothétique serveur Meta-Press.es pour être traitées. En fait c'est votre ordinateur qui apprend à faire les recherches pour vous. C'est votre navigateur web qui gagne le super-pouvoir d'interroger un millier de journaux en quelques secondes. En s'organisant comme ça, il est plus difficile de censurer quelque chose, d'une part parce qu'il y a beaucoup de journaux interrogés et d'autre part parce que c'est chaque utilisateur qui fait tourner le service. Et c'est comme ça que fonctionne l'internet : pas de point unique de passage où une défaillance entrainerait la défaillance du système. Plus il y a de personnes qui utilisent Meta-Press.es, mieux il fonctionne.

=== Les problèmes de la recherche sur Internet aujourd'hui

*Q : Quels problèmes voyez-vous avec les moteurs de recherche aujourd'hui ?*

R : Mon principal problème avec les moteurs de recherche des GAFAM est celui que j'ai mentionné en introduction : ils servent (aussi) des résultats provenant de faux journaux. Il est assez facile de forger du contenu sur mesure pour percer à travers leurs algorithmes de classement des résultats. Et comme personne ne vérifie jamais, on peut les leurrer. C'est à votre portée, et c'est à la portée des entreprises et des gouvernements. C'est troué, y'a besoin d'autre chose. Ces moteurs de recherche sont ballonnés de mauvais contenus, malhonnêtes, cassés en quelque sorte. Et ça ne va faire qu'empirer parce qu'il y a de l'argent en jeu et des enjeux politiques.

==== La surveillance de masse comme modèle économique

Un autre problème qu'on rencontre quand on veut lancer une recherche sur le net, c'est que le marché est dominé par une poignée de mégacorp' qui ne respectent aucune règle et dont le modèle économique est basé sur la surveillance de masse. Ils espionnent tout le monde, notant les habitudes de chacun (qui li quoi, qu'est-ce qui vous intéresse, où est-ce que vous avez cliqué…). Si on vous ajoutait un écran pour lister toutes les informations que les GAFAM arrivent à extraire de vos navigations sur le web, vous éteindriez tout simplement votre ordinateur. Et ces informations sont vendues aux plus offrants (entreprises, gouvernements, partis politiques…). On est pas dans de la théorie du complot là, je rappelle juste ce qu'on a appris grâce à Edward Snowden ou au scandale Cambridge Analytica. Ce serait plus confortable de vivre dans un monde où nous pourrions oublier ces scandales, mais ils constituent la réalité à laquelle nous devons faire front.

==== Déclin de la précision des moteurs de recherche

Un 3e problème vient du fait que les moteurs de recherche classiques perdent en précision. De plus en plus de contenu commercial sans grand intérêt est créé sur mesure pour sortir en premier sur les moteurs de recherche. Ils apparaissent même s'ils ne répondent pas vraiment à vos questions. Dans le domaine de la sécurité informatique on appelle ça une attaque par injection. Google peut être vu comme une faille de sécurité béante, ouverte aux attaques par injection. C'est un problème technique rencontré aujourd'hui par les moteurs de recherche.

==== Et l'esprit critique dans tout ça ?

Un dernier point concerne le fait que si les humains gagnent du temps à utiliser les moteurs de recherche, ils s'en trouvent également ramollis dans leurs compétences. La situation est comparable avec l'utilisation des GPS. En utilisant un GPS on perd lentement son sens de l'orientation (ou du moins l'habitude de préparer un trajet à l'aide d'une carte avant de prendre la route). Or si un GPS devenait malhonnête et imprécis, ne vous menant plus vraiment là où vous voulez aller footnote:[Mais par exemple systématiquement devant des centres commerciaux et en passant devant un maximum de pubs], vous abandonneriez l'outil. Les moteurs de recherche sont malhonnêtes et imprécis, il faut les abandonner. Vous devriez vous entrainer à rechercher des informations autrement : à croiser vos informations, à comparer vos sources, à publier le résultats de vos recherches pour que d'autres puissent vérifier à leur tour… Il s'agit de devenir un peu journaliste à son tour. Comme l'a dit Viktor Lofgren du projet Marginalia : plus vous utilisez Google et plus vous vous retrouvez enfermé dans le petit parc de ce que Google veut bien vous montrer. Le meilleur moteur de recherche, c'est votre cerveau !

=== L'information a un prix

*Q : Comment est-ce que vous résolvez ces problèmes avec Meta-Press ?*

R : Meta-Press.es répond au problème des _fake news_ en ne fournissant que des sources qui ont été reconnues et validées par des humains. Meta-Press ne cherche que dans de vrais journaux, avec des articles écris par de vrais humains. Des humains qui ont été entrainés et payés pour ça et qu'on appelle des journalistes. Ce type d'information a un prix, il faut bien le payer. Si vous ne payez pas votre information, c'est vous le dindon de la transaction. Et en plus vous ne recevez pas vraiment l'information que vous cherchiez (entre contenus publicitaires ou lubies nauséabondes de milliardaires en guerre civilisationnelle). Vous devriez donc payer pour les services que vous utilisez (ou alors les faire tourner sur votre machine). Si nous ne changeons pas la façon dont fonctionne le monde, il continuera à tourner de travers.

=== Un index décentralisé

Il y a plusieurs projets de logiciel libre qui travaillent à réaliser un moteur de recherche généraliste comme YaCy, Searx ou Marginalia. Ils s'attaquent à un problème difficile : comment co-construire un indexe distribué qui soit honnête et fiable ? Avec Meta-Press on a simplifié le problème en ne nous occupant que de recherche dans la presse. Les journaux en ligne fournissent généralement un index honnête de leur contenu, par le biais de leur recherche interne. Leur réputation serait entachée si leur moteur de recherche interne était mauvais. Meta-Press ne fait que rassembler ces index de journaux. C'est une petite fenêtre ouverte sur le monde, ça ne permet d'atteindre que ce que des journalistes ont écrit. Mais c'est une fenêtre qui donne sur une grosse partie du monde quand même, car "regarder un peu partout" c'est justement le métier des journalistes.

Créer un index de moteur de recherche généraliste est un problème compliqué et moi je suis meilleur pour résoudre les problèmes simples. Je me suis donc limité à un problème simple. Quand j'ai commencé, Meta-Press.es était tout petit comparé à Google, mais j'ai décidé de persévérer, de m'occuper de ce problème là (la recherche d'actu sur le web) et d'arracher ce cheveux de la tête du géant Google plutôt que de chercher à le terrasser à moi tout seul. Je vous invite à en faire de même, choisissez un combat, arrachez votre cheveux de la tête de Google. Attaquez-vous à un problème, et faîtes les choses bien.


=== Le futur de la recherche en ligne devrait être collaboratif

*Q : Que pensez-vous qu'il va se passer avec la recherche en ligne à l'avenir ?*

R : ChatGPT pourrait signifier la fin des moteurs de recherche comme Google d'ici un an ou deux si sa popularité continue de grimper à cette allure. Pas parce que les résultats obtenus sont meilleurs mais parce qu'il est plus simple à utiliser. ChatGPT n'est qu'un perroquet stochastique. Il n'a aucune idée de ce qu'il raconte. Vous pouvez lui demander la liste des présidents de la république française et vous obtiendrez une liste. Demandez lui quelles furent les présidentes et vous obtiendrez aussi une liste, alors qu'aucune femme n'a encore présidé la France. Malgré ça, ChatGPT cartonne parce qu'il est plus simple à utiliser pour les gens. Or c'est toujours ça qui gagne (le plus simple, pas le meilleur).

ChatGPT fonctionne grâce à un algorithme qui se revendique d'intelligence artificielle et heureusement pour nous ça n'a rien à voir avec l'intelligence artificielle des films Matrix ou Terminator. Il ne s'agit cette fois que d'une matrice de statistiques et cette technologie, vieille de 40 ans, peut aussi être utilisée pour le bien de l'humanité. Par exemple il existe https://plantnet.org/[Pl@ntNet], un site web où vous pouvez envoyer des photos de plantes et qui vous répondra à quelle espèce votre spécimen ressemble le plus. Voilà une porte ouverte vers le savoir. Il s'agit de la même technologie (que ChatGPT), mais utilisée de la bonne manière. iNaturalist.org vous aide, de la même manière, à identifier l'insecte qu'il y a en face de vous. BirdNet de l'université de Cornell vous dira quels oiseaux vous êtes entrain d'entendre (et avez réussi à enregistrer avec votre mobile).

Voilà des exemples de moteur de recherche en ligne qui m'enthousiasment. Il s'agit d'efforts collaboratifs. Plus ils sont utilisés et plus ils s'améliorent. Pl@ntNet par exemple, lorsqu'on lui envoi la photo d'une plante rare (quelque chose qui manque dans leur base de donnée), vous invite à envoyer plus de photos et à revenir lorsqu'il y aura des fleurs ou des fruits… De cette manière l'humanité travaille ensemble pour améliorer notre connaissance du monde. C'est à mon avis dans cette direction qu'il faut aller. Je ne vois pas l'intérêt d'aller sur Mars tant que nous n'aurons pas fini de cartographier le fond des océans.

=== Nous ne devrions pas nous reposer sur les moteurs de recherche pour connaître le monde

*Q : Est-ce la direction que vous souhaitez voir prendre à la recherche en ligne, devenir plus collaborative ?*

R : Oui. Il y a deux aspects au problème. Est-ce le moteur de recherche qui a besoin d'être amélioré, ou est-ce plutôt la façon dont nous publions les choses ? Chercher un livre dans une bibliothèque est facile parce que c'est un espace organisé. Il y a des étagères où les livres sont rangés par ordre alphabétique et ça marche très bien. Si nous publions mieux nos contenus, il ne sera pas compliqué de les rechercher. Mais espérer que les moteurs de recherche vont nous libérer de l'effort à faire pour connaître le monde est une illusion. L'outil s'interpose entre le savoir et vous, vous retenant prisonniers comme des passereaux englués (dans le respect de la tradition).

Il me semble préférable de garder en tête les limites du rêves qui nous est vendu : la soi-disante intelligence artificielle n'est qu'un algorithme de tri automatique des données qui n'a aucune idée de ce qu'il manipule. Les outils ne vont pas régler les problèmes de société. C'est aux humains de travailler ensembles.

Pour moi la direction à suivre est celle des efforts collaboratifs visant à découvrir et à cartographier le monde, comme OpenStreetMap. Des gens travaillant ensemble pour mettre leurs connaissances du monde en commun de manière à pouvoir chercher dedans ensuite. Vous pouvez par exemple aider Meta-Press.es à cartographier les journaux du monde. C'est aussi un effort collaboratif. Le projet est ouvert aux contributions. Aidez-nous à découvrir le monde ! Et grâce à la NLnet il y aura bientôt un formulaire permettant d'ajouter une source à Meta-Press en quelques clics (sans avoir besoin de connaissances en développement web).

=== Comment s'impliquer dans Meta-Press.es

*Q : Comment contribuer à Meta-Press.es ?*

R : Vous pouvez ajouter vos propres sources à Meta-Press. Sur le site du projet Meta-Press.es vous trouverez une documentation détaillant comment Meta-Press.es voit le monde et comment il peut lire vos journaux favoris. Pour l'instant c'est un processus assez long, accessible seulement à quelqu'un qui n'a pas peur du développement web. Mais grâce au financement de la NLnet je suis entrain de développer une interface qui vous permettra de copier simplement l'adresse d'un journal dans un formulaire, puis de pointer à la souris où est le moteur de recherche, puis où sont les résultats, leur lien, leur date et se sera déjà suffisant pour commencer.

Une fois que vous avez cette définition de journal fonctionnelle dans Meta-Press sur votre ordinateur, vous pouvez nous l'envoyer par courriel à l'adresse contact@ ou créer une demande d'ajout sur le FramaGit.org. Vous pouvez également nous joindre par IRC ou Mastodon.

Ajouter des sources est une contribution ouverte à tous, mais toute aide est la bienvenue. Vous pouvez aider le projet en aidant aux traductions (qui sont gérées via Weblate.org, un super logiciel libre portée par une équipe au modèle économique éthique). Vous pouvez aussi aider le projet en en parlant autour de vous. Présentez-le à vos amis, dans votre université, votre médiathèque… Meta-Press.es est configurable et permet de cibler ses recherches (texte, photo, évènements…), thème ou langue (parmi les 75 accessibles aujourd'hui). Essayez Meta-Press.es, utilisez-le et faîtes-le connaître.
