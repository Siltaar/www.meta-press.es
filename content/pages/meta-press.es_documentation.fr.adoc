= Documentation
:slug: meta-press.es_documentation
:lang: fr
:experimental: yes
:toc:
:toc-title: Index

include::pages/a_propos.fr.adoc[tag=what_is_it]

== Comment installer l'extension ?

*Pour installer Meta-Press.es* sur votre navigateur (Firefox ou dérivés, comme
le https://www.torproject.org/[Tor Browser]) : ouvrir la page suivante
https://addons.mozilla.org/fr/firefox/addon/meta-press-es/[addons.mozilla.org]
et cliquer sur le gros bouton bleu kbd:[+ Ajouter à Firefox].


== Comment s'en servir ?

Une fois installée, l'extension crée un bouton dans la barre d'outils, avec
l'icône de l'étoile filante de Meta-Press.es.

.bouton portant l'icône de Meta-Press.es
image::/images/doc/favicon-metapress-v2.png[]

Un clic sur ce bouton ouvre un nouvel onglet sur l'interface du moteur de
recherche.

Si l'icône n'est pas présente dans la barre d'outils, c'est probablement
qu'elle vous attend cachée derrière le menu « pièce de puzzle » des extensions
de votre navigateur. Il est alors possible de l'épingler pour l'avoir tout le
temps dans la barre d'outils.

Il se peut aussi que vous n'ayez pas autorisé l'extension à fonctionner en mode
"navigation privée" lors de l'installation alors que vous utilisez ce mode de
navigation. Dans ce cas, il est toujours possible de donner cette autorisation
en retrouvant Meta-Press.es dans la liste des _addons_ du navigateur
(accessible depuis son menu principal ou via l'adresse `about:addons`).

Profitez en pour vérifier que les mises à jour automatiques sont activés pour
bénéficier des dernières sources et fonctionnalités.

.interface du moteur de recherche
image::/images/doc/20210316_main_page_.png[]

Sous le nom de l'extension dans cet onglet, les gros titres des journaux
sélections sont chargés et défilent.

Vous pouvez saisir votre requête et choisir dans quels journaux chercher, grâce
au mécanisme multi-critères de filtrage des sources.

Les résultats sont ensuite listés en dessous quand ils arrivent.

Chaque résultats comporte un titre, un lien vers l'article, sa source, sa date,
et potentiellement un auteur et un extrait :

.détails d'un résultat
image::/images/doc/20191029_meta-press_result_.png[]

Les outils pour travailler sur ces résultats (tri, filtrage, sélection),
apparaissent dans la colonne à droite des résultats.

.nombre de résultats par source et filtrage par source, en cliquant sur le nom d'une source
image::/images/doc/20210316_results_.png[]


.filtrage par date
image::/images/doc/20171216_meta-press_country_.png[]

Vous pouvez par exemple cliquer sur le lien "_Toggle selection mode_" pour
faire apparaître une case à cocher pour chaque résultat et commencer à en
sélectionner. Vous pouvez ensuite exporter cette sélection en différents
formats (JSON, RSS, ATOM, et bientôt CSV).

.mode de sélection pour export
image::/images/doc/20191029_meta-press_europe_selection-mode_crop_circles.png[]

Pour ré-importer les résultats d'une recherche exportés dans un fichier,
cliquez sur le lien "_Import JSON_" (ou resp. RSS, ATOM) dans la barre
horizontale turquoise tout en haut et sélectionnez le fichier en question sur
votre disque dur.

=== Chercher dans une source précise

// tag::cherry_pick[]
Il est possible de choisir une par une les sources dans lesquelles ont souhaite chercher.

Pour cela, il faut déplier le panneau de recherche avancée, en cliquant sur le
[ + ] du titre "Recherche avancée [+]". Deux lignes apparaissent alors comportant
divers critères et une 3e ligne présente des boutons :

- remise à zéro des filtres,
- lister les sources,
- ajouter une source,
- automatiser une recherche.

Un clic sur le bouton "Liste des sources" fait apparaitre un 2e volet
permettant de parcourir la liste des sources, d'ajouter des sources à la
sélection courante ou d'en retirer.

L'icône de loupe, sur chaque ligne de source, permet de régler la prochaine
recherche sur cette source uniquement.

.Recherche avancée
image::/images/doc/20230414_advanced_search.png[]

// end::cherry_pick[]

=== Permaliens et marque-pages

// tag::permalinks[]
Lorsqu'une recherche est terminée une ligne de statistiques apparait au dessus
des premiers résultats. Cette ligne comporte une icône "maillons de chaîne"
&#128279; en fin de ligne. Cette icône est un lien permettant de lancer cette
recherche à nouveau.

.Icône "maillons de chaîne" du permalien
image::/images/doc/20210128_permalink.png[]

Il est ainsi possible de créer un marque page sur sa recherche favorite, sans
avoir à la configurer à chaque fois.
// end::permalinks[]

=== Recherches programmées
// tag::recherches_programmees[]

Une fois les termes de recherche saisis et la sélection des sources faites,
vous pouvez sauver ces réglages et programmer la recherche pour plus tard au
lieu de la lancer immédiatement. Il suffit pour ça de cliquer sur le bouton
&#9200; *Recherche programmée*. Ce bouton ouvre l'onglet des réglages sur la
partie réservée à la gestion des recherches programmées.

.Recherches programmées, avec interface en fond sombre
image::/images/doc/20210210_recherche_automatique.png[]

Ce tableau montre une ligne par recherche programmée. À sa création, une
recherche programmée est "Désactivée", mais il suffit de régler la date et
l'heure de sa prochaine exécution et de choisir sa périodicité pour qu'elle
s'active.

Vous pouvez ainsi programmer une recherche quotidienne en quelques clics.

Des actions sont possibles sur chaque recherche :

- le 1er bouton, avec un crayon &#9999;&#65039;, ouvre un onglet sur l'interface principale,
	configurée avec les paramètres de cette recherche (termes de recherche, choix
	des sources). Si vous modifiez ces réglages vous pouvez les sauver en
	cliquant sur le bouton de recherches programmées de l'interface principale ;
- le 2e bouton, avec une icône copier/coller permet de cloner une recherche
	programmée pour en faire une deuxième, configurée différemment ;
- le 3e bouton, avec une croix, permet de supprimer une recherche programmée ;
- le 4e et dernier bouton permet de lancer manuellement la recherche en
	question.
// end::recherches_programmees[]

== Comment ajouter une source au moteur de recherche ?

Pour ajouter une source à Meta-Press.es, le plus simple est d'utiliser le
formulaire de création de source en cliquant sur le bouton « Ajouter une source
» depuis l'interface principale. Un nouvel onglet s'ouvre alors sur un
formulaire vous guidant pas à pas le long de la démarche.

Ce formulaire ne fonctionne pour l'instant qu'avec les sources servant leurs
résultats en RSS, comme c'est le cas de la plupart des journaux basés sur
WordPress, le moteur le plus répandu (représentant presque la moitier des
sources de Meta-Press.es).

Si le formulaire ne reconnait pas de flux RSS de résultats pour la source que
vous voulez ajouter, il vous faudra alors vous penchez sur la 2e méthode,
recommandée aux programmeurs.

Si vous êtes un programmeur, ajoutez simplement une entrée dans le fichier
https://framagit.org/Siltaar/meta-press-ext[json/sources.json].

Une link:/fr/pages/meta-press.es_source_documentation.html[documentation exhaustive] a été rédigée pour vous aider dans cette tâche.
