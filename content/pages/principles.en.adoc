= Principles
:slug: principles
:lang: en
:experimental: yes
:toc:
:toc-title: Index

Version of : 2018-11-09

== Of the project

The Meta-Press.es project aims to follow the principles of the https://www.mozilla.org/en-US/about/manifesto/[Mozilla Manifesto].

01. The Internet is an integral part of modern life—a key component in education, communication, collaboration, business, entertainment and society as a whole.
02. The Internet is a global public resource that must remain open and accessible.
03. The Internet must enrich the lives of individual human beings.
04. Individuals’ security and privacy on the Internet are fundamental and must not be treated as optional.
05. Individuals must have the ability to shape the Internet and their own experiences on it.
06. The effectiveness of the Internet as a public resource depends upon interoperability (protocols, data formats, content), innovation and decentralized participation worldwide.
07. Free and open source software promotes the development of the Internet as a public resource.
08. Transparent community-based processes promote participation, accountability and trust.
09. Commercial involvement in the development of the Internet brings many benefits; a balance between commercial profit and public benefit is critical.
10. Magnifying the public benefit aspects of the Internet is an important goal, worthy of time, attention and commitment.

== Of the contributors

Inspired from https://framacolibri.org/guidelines[Framacolibri guidelines].

=== A civilized place for people to contribute together

Please treat this free software project with the same respect you would a public park. We, too, are a shared community resource — a place to share skills, knowledge and interests through ongoing commits and conversations.

These are not hard and fast rules, merely aids to the human judgment of our community. Use these guidelines to keep this a clean, well-lighted place for civilized public discourse.

=== Be agreeable, even when you disagree

You may wish to respond to something by disagreeing with it. That’s fine. But, remember to criticize ideas, not people. Please avoid:

*    Name-calling.
*    Ad hominem attacks.
*    Responding to a post’s tone instead of its actual content.
*    Knee-jerk contradiction.

Instead, provide reasoned counter-arguments that improve the conversation.

=== Your Participation Counts

The conversations we have here set the tone for everyone. Help us influence the future of this community by choosing to engage in discussions that make this project an interesting place to be — and avoiding those that do not.

Let’s try to leave our park better than we found it.

=== Always Be Civil

Nothing sabotages a healthy project like rudeness:

*    Be civil. Don’t post anything that a reasonable person would consider offensive, abusive, or hate speech.
*    Respect each other. Don’t harass or grief anyone, impersonate people, or expose their private information.
*    Respect our communication spaces. Don’t post spam or otherwise vandalize them.

These are not concrete terms with precise definitions — avoid even the appearance of any of these things. If you’re unsure, ask yourself how you would feel if your post was featured on the front page of the one of the newspapers we work on.

This is a public project, and search engines index it. Keep the language, links, and images safe for family and friends.

=== Keep It Tidy

Make the effort to put things in the right place, so that we can spend more time coding or debating and less cleaning up. So:

*    Only open an issue after a sound search of its equivallent among already open ones.
*    Don’t cross-post the same thing in multiple issues.
*    Don’t post no-content replies.
*    Don’t divert a topic by changing it midstream.


== Of the indexed newspapers

Meta-Press.es is only showing publicly available results of indexed newspapers. The content of the articles is still distributed by their respective owner's websites.

Meta-Press.es will be a neutral tool, and will come with a default list of
newspapers to search in. Every user will be able to load other lists of indexed
newspapers, make its own lists and offer them to others (indexing a newspaper
requires only 2 URL and 5 CSS selectors packed in a JSON object).

Sources of the built-in list must provide verified and verifiable factual information.

=== Classification

Here are suggested classifications, don't hesitate to suggest others.

==== Thematic classification

Current themes includes :

* regional / national / international
* financial / politics / ecology / media / technology / documentary
* science

==== Technical classification

Filters about those technical criterion will be provided to users.

* HTTPS accessible newspapers ;
* Newspapers with third-party search engine ;
* Fast vs Slow newspapers (fast for page loading time < 2s) ;
* And-logic newspaper search engines ;
* Nature : newspaper, magazine, scientific press, radio…

==== Cultural classification

* Language
* Country
