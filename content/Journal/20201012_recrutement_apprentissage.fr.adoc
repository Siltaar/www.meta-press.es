= Recrutement 2020 pourvu et v1.5.2
:slug: recrutement_2020
:lang: fr
:date: 2020-10-12
:author: Siltaar

Deux mois après avoir publié l'annonce, le projet Meta-Press.es est heureux
d'accueillir https://framagit.org/Christo26.g[Christopher Gauthier] en tant
qu'apprenti en alternance. Il travaillera 3 jours par semaine sur Meta-Press.es
pendant un an.

Mais ce n'est pas tout puisqu'il sera aidé, durant jusqu'à 6 mois, par
https://framagit.org/Remsi[Rémi Plancher] recruté comme stagiaire en alternance
selon une disposition exceptionnelle du gouvernement cette année permettant aux
candidats à l'alternance de commencer leur année par 6 mois de stage s'ils
n'ont pas encore trouvé de contrat d'apprenti.

Tous deux ont commencé leur travail par l'ajout de nouvelles sources à
Meta-Press.es, qui en totalise aujourd'hui plus de 200, pour 53 pays et en 22
langues.

La *version 1.5.2* publiée aujourd'hui comporte également quelques nouveautés
dans le format de définition des sources, comme :

- la gestion des encodages de caractère pour les sources qui ne servent pas
	leurs pages en UTF8 (il aura fallu en traiter 200 pour en trouver une…
	DailyNK) ;
- la définition explicite du domaine de base d'une source (pour les cas où un
	sous-dossier doit être ajouté aux URL relatives de la source) ;
- ou encore la déclaration d'URL de redirection, afin de pouvoir demander les
	permissions d'hôte correspondantes, dans le cas où une requête doit rebondir
	sur un domaine tiers pour aboutir sur le domaine principal (Daily Telegraph).

De la
https://www.meta-press.es/pages/meta-press.es_documentation.html#_redirections[documentation]
sur ces points a été ajoutée, et il y en a encore en cours de rédaction.

Cette version mets par contre l'annonceur automatique de mises à jour en pause
pour l'instant. En effet, le mécanisme s'appuyait sur l'API `localStorage`, or
ce dernier est perdu à la fermeture du navigateur en cas de navigation privée
(ce qui est le cas par défaut avec le TorBrowser par exemple), ce qui
entrainait une annonce à chaque lancement… Désolé pour le désagrément. À
l'avenir, cette fonctionnalité s'appuiera sur l'API `browser.storage` qui elle
est conservée (du moins encore avec Firefox 83.0a1).

Ensuite, un début de travail sur l'accessibilité de l'extension a été
entrepris. Pour l'instant cela se traduit par de meilleurs contrastes sur les
textes grâce notamment à l'outil
https://addons.mozilla.org/en-US/firefox/addon/wcag-contrast-checker/[WCAG
contrast checker], mais de nombreuses autres recommandations ont été formulées
suite à l'audit de Meta-Press.es par la https://www.accessibility.nl[Team
Accessibility Foundation] via https://www.ngi.eu[NGI0].

Enfin, Christopher travaille activement cette semaine au remplacement de la
bibliothèque de fonction https://github.com/Mobius1/Selectr/[Selectr] par
https://github.com/jshjohnson/Choices[Choices.js], afin de rendre le mécanisme
de sélection des sources fonctionnel sous Android.
