= Version 1.7.7 : security audit
:slug: v1.7.7_security_audit
:lang: en
:date: 2021-08-11
:author: Siltaar

As a last step planned in late 2019 and announced
https://www.meta-press.es/journal/2020/funds-from-the-nlnet-foundation.html[here],
the https://nlnet.nl[NLnet] (via the NGI0 consortium which was granted funds
from the European Horizon 2020 project) and the
https://radicallyopensecurity.com[RadicallyOpenSecurity] company was providing
Meta-Press.es a security audit.

The new v1.7.7 release is the result of this audit, with mainly (small)
security improvements.

After some delays (in months) in the planning of the audit, everything went
smoothly. First I provided credentials to get an account in
RadicallyOpenSecurity infrastructure (including a private GitLab repository
hosting the audit results and a web chat).

Then came some more waiting (in weeks) for a penetration tester to select
Meta-Press.es for its next task (and reviewing a WebExtension seemed not so
common). Once a courageous pen.-tester showed up, we started with a
video-conference during which we discussed about what Meta-Press.es is and
what it intends to do. I was also asked about what potential security issues I
would foresee and I was pleased to list points I was wanting to get checked.

Results came during the next weeks with some text chat to keep heading in the
right direction. The collaboration was efficient : I learnt a lot, was happy to
get mainly good results and finally quite occupied with the effective
findings to fix and the recommendations to implement.

The methodology was simple : inspect the castle walls. What comes in, what goes
out. All the dependencies were checked, and then the data from fetched sources
(with an elaborated network frame inspection setup) and the exported files.

The basics principles of Meta-Press.es were confirmed (no third-party trackers
are activated while using Meta-Press.es), source data were correctly sanitized
(except in the exotic scenario of JSON-responding sources, which is fixed by
this release) and the recommended Two-Factor Authentication (2FA) was already
activated for all online services Meta-Press.es relies on (domain name
registration, web hosting, Mozilla Addons repository…).

I was advised to further document intended behavior regarding security aspects
in addition to how-to report security issues to the project. Stricter
Content Security Policy (CPS) rules were also advised as this was already my
naïve implementation of this mechanism that got me covered against severe issues
in the JSON-responding sources scenario. A fire-wall approach with everything
disabled by default and only what's needed allowed was elected and implemented
(which required quite some work to avoid inline CSS for instance).

To finish with security, I've been introduced to *ReDOS* attacks : denial of
service through regular expression slow edge-case feeding. Now on,
Meta-Press.es source-definitions will be tested against Nicolaas Weidman
https://github.com/NicolaasWeideman/RegexStaticAnalysis[RegexStaticAnalysis]
tool (despite the fact that it's written in Java :-p).

Then, as I updated dependencies I activated the CodeMirror `json-lint` plugin
which allows to underline malformed JSON content in the source creation
textarea. I also traded 15 small try/catch blocks in the code against one big
that ensure every searches will now have an end (even if a source is causing a
bug in Meta-Press.es with an unanticipated answer). Try/catch blocks were
non-optimizable portions of code since a long time but things have evolved. Let
me know if you hit measurable performance penalties.

A last word about statistics, we can guess that the majority of the users are
French (not only because Mozilla reports 75% of french-speaking users, or
because I mainly presented Meta-Press.es to french audiences) but also because
statistics are dramatically dropping during holidays : from 800 users a day to
550 in mid-august.

Fortunately, the number of sources is still growing, with currently 310 !

PS: A new way of supporting Meta-Press.es has recently been introduced, it's
https://www.helloasso.com/associations/meta-press-es/formulaires/1/en[HelloAsso].
It allows one shot donations and to get 100% of your donation sent to the new
Meta-Press.es non-profit association (if you click to remove their self-added
tip). For the record, the complete list of how to support Meta-Press.es is
available
https://www.meta-press.es/pages/a_propos.html#_you_can_support_this_work[here].
