= Meta-Press.es at the DECODE symposium in Turin
:slug: decode_symposium_turin
:lang: en
:date: 2019-10-24
:author: Siltaar

Two weeks before the event, the planning of the DECODE symposium in Turin is out :

https://decodeproject.eu/events/workshops-agenda-5th-november

I will present Meta-Press.es in the last tech talk on november the 5th (at 17h50).

Demonstration should take place at the Meta-Press.es stand in the village of associations.
