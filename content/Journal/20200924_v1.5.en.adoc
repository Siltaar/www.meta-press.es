= Release of Meta-Press.es v1.5
:slug: release_v1.5
:lang: en
:date: 2020-09-24
:author: Siltaar

After a long summer break, here is the new version of Meta-Press.es.

This version fulfills a new development step of link:/journal/2020/funds-from-the-nlnet-foundation.html[NLnet support]: Better Firefox integration and fewer asked permissions.

This versions also add off-the-roadmap cool features, such as optional photo
loading in results, or possible filter-out of results from a particular source
once the search is done (to clear out garbage results from a broken source for
instance).

Here is the detail of delivered improvements since v1.4 :

- New links in the interface :
** Add a local permalink for the searches (the link icon 🔗 at the end of the search stats). This link allows to create a bookmark to quickly perform the same search again (search terms, source selection) ;
** Add an "arrow" (↗) link to the sources search-page, to browse results directly at the source, and get older results for instance ;
** Add a "cross" (✘) link allowing to remove results of a particular source among the search results, to clean up results from a deceptive source ;
** Add a "reload headlines" link (↻) ;
- Behavior improvements :
** Direct display of photos in results, and new "photo" result type (+ setting to only load the pictures on demand, clicking on them, and spare bandwidth) ;
** Improved keyboard navigation for accessibility purpose ;
** Dynamic update of the tab title according to search stats ;
- Better Firefox integration :
** Browser notification for long (>8s) searches footnote:[If `requireUserInteraction` is set to its default 0 in your `about:config`] as per issue https://framagit.org/Siltaar/meta-press-ext/-/issues/20[20] ;
** Ask for fewer permissions : ask for host permissions at search time and give control to user (drop them, request all, keep them or not) as per issue https://framagit.org/Siltaar/meta-press-ext/-/issues/33[33] ;
- Technical improvements :
** Remove `unsafe-eval` authorisation from `manifest.json` ;
** Evolution of the source description format with field aggregation now possible via `_tpl` field suffix (which is a template combining other field values for this field) ;
- 25 new sources :
** Reference press (9) : Estadao, Folha de S.Paulo, El Mercurio, O Globo, Al-Ahram, Helsingin Sonomat, The Times, Irish Times…
** French local press and magazines (16) : La Croix, TV5Monde, Bastamag, L'age de faire, Le Midi Libre, Le progrès, L'Alsace, Le Bien Public, DNA (Dernière nouvelles d'Alsace), L'est républicain, Le républicain lorrain, Vosges Matin…
** Video sources : TV5Monde, Le Figaro Vidéos, Le Midi Libre ;
** Photo sources : 11 sources have been marked with the "photo" result type as a first step.

PS: v1.5.1 fixes a bug about the default tags and adds 20 more Reference Press sources. It also adds an update notification mechanism that opens up this Meta-Press.es official website at each updates to allow finding out what's new.
