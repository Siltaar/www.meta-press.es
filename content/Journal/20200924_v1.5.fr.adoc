= Sortie de Meta-Press.es v1.5
:slug: release_v1.5
:lang: fr
:date: 2020-09-24
:author: Siltaar

Après une longue pause estivale, voici la nouvelle version de Meta-Press.es.

Cette version permet d'atteindre une nouvelle étape du link:/journal/2020/funds-from-the-nlnet-foundation.html[partenariat avec la NLnet] : une meilleure intégration à Firefox et moins de permissions demandées.

Cette version ajoute également quelques fonctionnalités non planifiées mais
fort sympathiques, comme le chargement optionnel des images dans les résultats,
ou la possibilité de retirer les résultats d'une source donnée, au cas où cette
source donnerait de mauvais résultats.

Voici le détail des améliorations depuis la v1.4 :

- Nouveaux liens dans l'interface :
** Ajout d'un permalien local pour les recherches (l'icône de chaîne 🔗 à la fin de la ligne de statistiques d'une recherche). Ce lien permet par exemple de créer un marque page pour relancer ensuite directement la recherche en question (mots clés, sélection de sources…) ;
** Ajout d'un lien "flèche" (↗) pointant vers la page de recherche d'une source, pour consulter les résultats directement sur le site de cette source et ainsi avoir accès aux résultats plus anciens ;
** Ajout d'un lien "croix" (✘) permettant de retirer les résultats d'une source en particulier, et nettoyer ainsi sa recherche, si cette source ne donnait pas satisfaction ;
** Ajout d'un lien (↻) pour recharger les gros titres (utile si on a décoché le rechargement automatique par exemple) ;
- Comportements améliorés :
** Affichage des photos dans les résultats, et ajout d'un type de résultat "photo" (et d'un réglage pour désactiver le chargement automatique des photos, le rendant manuel pour économiser de la bande passante) ;
** Amélioration de la navigation au clavier pour soigner l'accessibilité ;
** Mise à jour dynamique du titre de l'onglet en fonction des statistiques de la recherche en cours ;
- Meilleure intégration à Firefox :
** Notification, par le navigateur, de la fin d'une longue recherche (>8s) footnote:[Si le paramètre `requireUserInteraction` vaut bien 0 dans `about:config`] comme annoncé https://framagit.org/Siltaar/meta-press-ext/-/issues/20[ici] ;
** Réduction drastique des permissions requises : les permissions d'hôte sont désormais demandées au lancement d'une recherche, seulement pour les domaines visés par la recherche, et l'utilisateur a la possibilité de retirer toutes les permissions dans les réglages, de donner toutes les permissions d'un coup et choisir si les permissions doivent être conservées ou non entre deux recherche, comme étudié https://framagit.org/Siltaar/meta-press-ext/-/issues/33[là] ;
- Améliorations techniques :
** Retrait de l'autorisation `unsafe-eval` du `manifest.json` ;
** Évolution du format de description des sources, avec la possibilité d'agréger plusieurs champs via un mécanisme de `template` (`_tpl`) combinant les valeurs de plusieurs champs dans un autre champs ;
- 25 nouvelles sources :
** Presse de référence (9) : Estadao, Folha de S.Paulo, El Mercurio, O Globo, Al-Ahram, Helsingin Sonomat, The Times, Irish Times…
** Presse locale française et magazines (16) : La Croix, TV5Monde, Bastamag, L'age de faire, Le Midi Libre, Le progrès, L'Alsace, Le Bien Public, DNA (Dernière nouvelles d'Alsace), L'est républicain, Le républicain lorrain, Vosges Matin…
** Sources vidéos : TV5Monde, Le Figaro Vidéos, Le Midi Libre ;
** Sources photos : 11 sources ont été étiquetées avec le type de résultat "photo" pour commencer sur le sujet.

PS: la v1.5.1 règle un petit problème avec la sélection de sources par défaut et ajoute 20 sources de plus dans la catégorie Presse de Référence. Cette version ajoute également un mécanisme de signalement des mises à jour, qui ouvre le site officiel de Meta-Press.es à chaque mise à jour pour informer des nouveautés.
