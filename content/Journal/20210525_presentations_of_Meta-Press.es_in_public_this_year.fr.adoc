= 3 pr&eacute;sentations de Meta-Press.es en public cette ann&eacute;e
:slug: 2021_presentations_of_Meta-Press.es_in_public
:lang: fr
:date: 2021-07-28
:author: Siltaar

Trois présentations publiques de Meta-Press.es sont prévues (ou en
candidatures) pour avant la fin de l'année :

- 2021-10-14 15:00 UTC+2 à La Rochelle : https://b-boost.fr/[*B-Boost.fr*]
- 2021-10-21 11:00 UTC+2 à Niort : https://www.forum-ess.fr/?Contributions2021#collapse2_MetaPressEsOutilEcologiqueDeRechercheDa[*6th Forum ESS*] (https://www.forum-ess.fr/?Programme2021Jeudi&facette=listeListeThemaprog2021=Ecologie|listeListeSecteur2021=PD|listeListeFormatcontribution=Inspiration[dans le programme])
- [.line-through]#2021-11-09 --:-- à Paris : Open Source Experience#
