= Conférences autour de Meta-Press.es à Lyon pour les JdLL 2023
:slug: 2023_JDLL
:lang: fr
:date: 2023-03-14
:author: Siltaar

Après une présentation en bonne et due forme link:/journal/2022/2022_JDLL.html[l'an dernier] Meta-Press.es sera encore présent aux Journées du Logiciel Libre en 2023 de Lyon avec pas moins de *2 conférences* !

Cette manifestation s'inscrit dans le cadre de l'opération
https://www.libre-en-fete.net/2023/[Libre en Fête 2023] partout en France à
partir du 20 mars.

Vous pourrez me retrouver ici :

- 2023-04-01 14:00 UTC+2 à Lyon : https://pretalx.jdll.org/jdll2023/talk/review/3EKJQHH7GBP9GFQXTUZPJPZNDNQ7BYCQ[Financement du logiciel libre par l'Europe : 3 ans de soutien à Meta-Press.es] (salle des cultures)
- 2023-04-02 13:00 UTC+2 à Lyon : https://pretalx.jdll.org/jdll2023/talk/review/PZ99XPTQVWNV9SHLZTTTUKEGKLR3RRTF[Web scrapping : lessons tirées de l'intégration de 675 sources à Meta-Press.es] (salle des possibles)


Programme complet : https://jdll.org/programme

== Financement du logiciel libre par l'Europe : 3 ans de soutien à Meta-Press.es

image::https://pretalx.jdll.org/media/jdll2023/submissions/EUYG38/NGI-NLnet-Meta-Press.es_k3zG7b3.png[title="NGI > NLnet > Meta-Press.es logos"]

Découvrez avec quelle simplicité et quelle efficacité la NLnet peut soutenir votre logiciel libre.

Quelques courriels, beaucoup de sérieux et des dizaines de milliers d'euros… Voilà à quoi se résume le tour de force qu'opère la NLnet en tant qu'intermédiaire entre la Commission Européenne (via son programme Next Generation Internet) et le fourmillant monde du logiciel libre.

Votre projet ouvre de nouveaux usages du net ? Il est éthiquement en logiciel libre ? Vous ne perdrez probablement pas de temps à répondre à un appel à candidature de la NLnet. C'est simple, direct et efficace… surtout en comparaison avec les 2 années de parcours du combattant pour obtenir la même chose de l'administration d'une grande région française (également décrites dans cette conférence).

== Web scrapping : lessons tirées de l'intégration de 675 sources à Meta-Press.es

image::https://pretalx.jdll.org/media/jdll2023/submissions/JW8KFL/XML_Parsing_Error_unfVKI0.png[title="Capture d'écran d'un message d'erreur corriace de la console développeur de Mozilla Firefox"]

Florilège de bizarreries qu'on croise vraiment sur le web…

Analyser une page web à partir de JavaScript dans une WebExtension, ça avait l'air simple pourtant !

Oui, mais le serveur il répond du JSONP en fait et le content-type ment comme un patient du DrHouse ! En plus y'a d'abord une redirection et puis le charset, bah c'est pas UTF8 … T_T

Florilège de ce que l'humanité fait vraiment du web dans 64 pays et 71 langues…
