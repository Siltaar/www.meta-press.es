= Video of the Meta-Press.es' presentation for Parinux
:slug: 2021_video_presentation_of_meta-press.es_for_Parinux
:lang: en
:date: 2021-04-20
:author: Siltaar

The Parinux association just uploaded the video of the presentation of
Meta-Press.es held for their "Free software evening chat" of the thursday 15th
of april 2021.

https://tube.fdn.fr/videos/watch/9ae43c36-3dc8-4d85-844b-c5a5335d77ed[*Meta-Press.es
: decentralized and ecological search engine protecting your privacy, by Simon D.*] (1h 28min 24sec) (_fr_)

More than 160 people attended Meta-Press.es presentations so far and there were
600 downloads during the last 90 days.

By the way, the next presentation will take place the wednesday *21st of april
2021 at 18h00* (Paris time) on the impulsion of
https://montpellibre.fr/[Montpel'Libre] via Meet Jit.si.
https://meet.jit.si/webinaire_montpellibre_vienumerique[*Click here to join*].
https://montpellibre.fr/spip.php?article5141[Click here for more info…] (_fr_)
