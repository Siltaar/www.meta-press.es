= Version 1.8.14 : maintenance des sources et étiquettes access content
:slug: v1.8.14_global_source_maintenance_access_content_and_direct_content
:lang: fr
:date: 2024-04-04
:author: Siltaar
:experimental: yes
:toc:
:toc-title: Index

Puisque j'en était à faire de la maintenance sur le sources pour vérifier la corretion d'un bug sur l'interprétation des dates, je me suis lancé dans un sprint de maintenance globale des sources, en me penchant sur chaque source signalée comme en panne par la procédure de test automatisée.

== Grand ménage de printemps pour les sources

Ça a demandé beaucoup de travail… des centaines de sources ont été réparées. J'en ai également profité pour redonner une chances aux sources marquées comme cassées dans le fichier `json/sources.json` et 15 d'entres elles sont de nouveau fonctionnelles (dont certaines qui avaient besoin du encore nouveau mécanisme de récupération préliminaire de jeton).

//  git diff --compact-summary 09b7c7650b37b0ea3112d9357d2ea12f30b7e1e2 json/sources.json
Cette petite tâche de maintenance s'est muée en remuage de millier de lignes :

    2743 insertions(+), 1824 deletions(-)

Le tout prenant un peu plus de temps que prévu.

Mais le résultat est là, Meta-Press.es compte désormais 953 sources fonctionnelles, dont 510 sources francophones ce qui est, pour rappel, plus que Google Actualités (qui annonce se limiter à 500 sources).

Le nombre de sources dîtes "approximatives" (avec lesquelles il est difficile de travailler parce qu'elle renvoient un peu n'importe quoi) est repassé sous la barre des 100 (à 93) grâce à la qualitication de nombreuses sources en "one word" (précise pour au moin un mot) via le mécanisme `filter_results` présenté précédemment.


== Étiquettes `access content` et `direct content`

Toutefois l'amélioration principale de cette version c'est l'apparition des étiquettes : `access content` et `direct content`.

La première permet de ne chercher que dans les sources présentant un contenu accessible en ligne : vraiment sur le web, sans _paywall_ in inscription obligatoire. Toutes les sources de Meta-Press.es ont été vérifiée et 692 d'entre elles se sont révélées coopératives, soit 72% des sources de Meta-Press.es. Ce qui était jusque là vu comme une faiblesse de l'outil (ne pas donner accès au contenu des articles) se révèle finalement être une force : dans la majorité des cas Meta-Press.es renvoi vers du contenu lisible.

Et ce n'est pas tout, il est apparu que 127 sources poussent en pratique l'intégralité de leur contenu sur demande, ce qui permet à Meta-Press.es de présenter des articles entiers à lire dans ses pages. Ces contenus peuvent également être exportés, portagés et archivés. Ces sources on reçu l'étiquette `direct content`.

Ces deux étiquettes sont pour l'instant à retrouver dans les étiquettes "techniques" mais pourrait bientôt avoir leur propre catégorie.


== Suivi de l'age des ligne dans `json/sources.json`

Vu le temps qu'a pris cette maintenance des sources et le travail que ça représente, j'ai fait un petit script permettant de retrouver l'age des lignes, année par année. En utilisant bien ce script il permettra de suivre le volume de maintenance nécessaire entre deux séances d'ajout de source.

Pour le moment il permet déjà de se faire une idée du travail accompli sur ce fichier chaque année.

|===
| Année | lignes
| 2019 | 598
| 2020 | 924
| 2021 | 3177
| 2022 | 7024
| 2023 | 8895
| 2024 | 3111
|===

Ce fichier comportant un total de 23 718 lignes, toutes écrites à la main par des humains.
